webpackJsonp([1],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Element = function () {
    function Element(selector) {
        _classCallCheck(this, Element);

        Object.assign(this, { el: (0, _jquery2.default)(selector), selector: selector });
    }

    _createClass(Element, [{
        key: 'show',
        value: function show() {
            this.el.show();
            return this;
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.el.hide();
            return this;
        }
    }]);

    return Element;
}();

exports.default = Element;

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

var _browserEventEmitter = __webpack_require__(2);

var _browserEventEmitter2 = _interopRequireDefault(_browserEventEmitter);

__webpack_require__(16);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Select = function (_Element) {
    _inherits(Select, _Element);

    function Select(selector) {
        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
        var selectText = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'Выберите';
        var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { width: '209px' };

        _classCallCheck(this, Select);

        var _this = _possibleConstructorReturn(this, (Select.__proto__ || Object.getPrototypeOf(Select)).call(this, selector));

        Object.assign(options, { selectText: selectText, onSelected: function onSelected(_ref) {
                var selectedData = _ref.selectedData;
                return _this.touch(Object.assign(_this, { selectedData: selectedData }));
            }, truncateDescription: true });
        Object.assign(_this, { events: new _browserEventEmitter2.default(), options: options });
        _this.init(data);
        return _this;
    }

    _createClass(Select, [{
        key: 'touch',
        value: function touch() {
            this.events.emit('selected', this.data);
            return this;
        }
    }, {
        key: 'on',
        value: function on(eventName, callback) {
            this.events.on(eventName, callback);
            return this;
        }
    }, {
        key: 'close',
        value: function close() {
            this.el.ddslick('close');
            return this;
        }
    }, {
        key: 'select',
        value: function select(index) {
            if (this.options.data.length && this.options.data[index]) {
                (0, _jquery2.default)(this.selector).ddslick('select', { index: index });
                this.touch(this.selectedData = this.options.data[index]);
            }
            return this;
        }
    }, {
        key: 'init',
        value: function init() {
            var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

            Object.assign(this.options, { data: data });
            //bug fix
            (0, _jquery2.default)(this.selector).ddslick('destroy');
            // console.log(this.constructor.name, this.el.attr('id'), data);
            (0, _jquery2.default)(this.selector).empty();
            (0, _jquery2.default)(this.selector).ddslick(this.options);
            return this.select(index);
        }
    }, {
        key: 'data',
        get: function get() {
            return this.selectedData || (0, _jquery2.default)(this.selector).data('ddslick') || { value: null };
        }
    }, {
        key: 'value',
        get: function get() {
            return this.data.value;
        }
    }]);

    return Select;
}(_element2.default);

exports.default = Select;

/***/ }),
/* 4 */,
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = {"2":{"name":"МИНИ","description":"297 мм","sizes":{"1":{"text":"МИНИ","value":"1","blocks":{"2":{"src":"/images/calendar/block/2.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"3":{"text":"Нет","value":"3","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"4":{"text":"1 поле","value":"4","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"8":{"text":"Нет","value":"8","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"10":{"text":"1 поле","value":"10","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"12":{"text":"3 поля","value":"12","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"2","text":"ПАРТНЕР Стандарт Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3080},"3":{"src":"/images/calendar/block/3.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"5":{"text":"Нет","value":"5","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"6":{"text":"1 поле","value":"6","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"7":{"text":"Нет","value":"7","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"9":{"text":"1 поле","value":"9","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"11":{"text":"3 поля","value":"11","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"3","text":"ПАРТНЕР Стандарт Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3080},"4":{"src":"/images/calendar/block/4.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"13":{"text":"Нет","value":"13","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"14":{"text":"1 поле","value":"14","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"15":{"text":"Нет","value":"15","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"16":{"text":"1 поле","value":"16","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"17":{"text":"3 поля","value":"17","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"4","text":"Супер Болд Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3080},"5":{"src":"/images/calendar/block/5.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"20":{"text":"Нет","value":"20","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"21":{"text":"1 поле","value":"21","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"22":{"text":"Нет","value":"22","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"23":{"text":"1 поле","value":"23","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"24":{"text":"3 поля","value":"24","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"5","text":"Супер Болд Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3080},"6":{"src":"/images/calendar/block/6.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"27":{"text":"Нет","value":"27","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"28":{"text":"1 поле","value":"28","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"29":{"text":"Нет","value":"29","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"30":{"text":"1 поле","value":"30","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"31":{"text":"3 поля","value":"31","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"6","text":"Супер Болд Зеленый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"4","price":3080},"7":{"src":"/images/calendar/block/7.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"48":{"text":"Нет","value":"48","block_price":"32.50","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"49":{"text":"1 поле","value":"49","block_price":"32.50","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"50":{"text":"Нет","value":"50","block_price":"32.50","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"51":{"text":"1 поле","value":"51","block_price":"32.50","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"52":{"text":"3 поля","value":"52","block_price":"32.50","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"7","text":"Вердана Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3250},"8":{"src":"/images/calendar/block/8.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"53":{"text":"Нет","value":"53","block_price":"32.50","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"54":{"text":"1 поле","value":"54","block_price":"32.50","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"55":{"text":"Нет","value":"55","block_price":"32.50","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"56":{"text":"1 поле","value":"56","block_price":"32.50","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"57":{"text":"3 поля","value":"57","block_price":"32.50","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"8","text":"Вердана Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3250},"9":{"src":"/images/calendar/block/9.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"60":{"text":"Нет","value":"60","block_price":"32.50","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"61":{"text":"1 поле","value":"61","block_price":"32.50","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"62":{"text":"Нет","value":"62","block_price":"32.50","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"63":{"text":"1 поле","value":"63","block_price":"32.50","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"64":{"text":"3 поля","value":"64","block_price":"32.50","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"9","text":"Вердана Белый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"1","price":3250},"14":{"src":"/images/calendar/block/14.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"34":{"text":"Нет","value":"34","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"35":{"text":"1 поле","value":"35","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"36":{"text":"Нет","value":"36","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"37":{"text":"1 поле","value":"37","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"38":{"text":"3 поля","value":"38","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"14","text":"Классика Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3080},"15":{"src":"/images/calendar/block/15.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"41":{"text":"Нет","value":"41","block_price":"30.80","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"42":{"text":"1 поле","value":"42","block_price":"30.80","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"43":{"text":"Нет","value":"43","block_price":"30.80","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"44":{"text":"1 поле","value":"44","block_price":"30.80","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"45":{"text":"3 поля","value":"45","block_price":"30.80","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"15","text":"Классика Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3080},"16":{"src":"/images/calendar/block/16.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"95":{"text":"Нет","value":"95","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"96":{"text":"1 поле","value":"96","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"97":{"text":"Нет","value":"97","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"98":{"text":"1 поле","value":"98","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"99":{"text":"3 поля","value":"99","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"16","text":"ПАРТНЕР Дизайн Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"17":{"src":"/images/calendar/block/17.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"102":{"text":"Нет","value":"102","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"103":{"text":"1 поле","value":"103","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"104":{"text":"Нет","value":"104","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"105":{"text":"1 поле","value":"105","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"106":{"text":"3 поля","value":"106","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"17","text":"ПАРТНЕР Дизайн Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"18":{"src":"/images/calendar/block/18.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"109":{"text":"Нет","value":"109","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"110":{"text":"1 поле","value":"110","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"111":{"text":"Нет","value":"111","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"112":{"text":"1 поле","value":"112","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"113":{"text":"3 поля","value":"113","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"18","text":"ПАРТНЕР Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"19":{"src":"/images/calendar/block/19.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"116":{"text":"Нет","value":"116","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"117":{"text":"1 поле","value":"117","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"118":{"text":"Нет","value":"118","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"119":{"text":"1 поле","value":"119","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"120":{"text":"3 поля","value":"120","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"19","text":"ПАРТНЕР Болд Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"20":{"src":"/images/calendar/block/20.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"235":{"text":"Нет","value":"235","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"236":{"text":"1 поле","value":"236","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"237":{"text":"Нет","value":"237","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"238":{"text":"1 поле","value":"238","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"239":{"text":"3 поля","value":"239","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"20","text":"ПАРТНЕР Металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"21":{"src":"/images/calendar/block/21.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"242":{"text":"Нет","value":"242","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"243":{"text":"1 поле","value":"243","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"244":{"text":"Нет","value":"244","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"245":{"text":"1 поле","value":"245","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"246":{"text":"3 поля","value":"246","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"21","text":"ПАРТНЕР Металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"22":{"src":"/images/calendar/block/22.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"249":{"text":"Нет","value":"249","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"250":{"text":"1 поле","value":"250","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"251":{"text":"Нет","value":"251","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"252":{"text":"1 поле","value":"252","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"253":{"text":"3 поля","value":"253","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"22","text":"ПАРТНЕР Болд Металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"23":{"src":"/images/calendar/block/23.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"256":{"text":"Нет","value":"256","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"257":{"text":"1 поле","value":"257","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"258":{"text":"Нет","value":"258","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"259":{"text":"1 поле","value":"259","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"260":{"text":"3 поля","value":"260","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"23","text":"ПАРТНЕР Болд Металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"24":{"src":"/images/calendar/block/24.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"123":{"text":"Нет","value":"123","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"124":{"text":"1 поле","value":"124","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"125":{"text":"Нет","value":"125","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"126":{"text":"1 поле","value":"126","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"127":{"text":"3 поля","value":"127","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"24","text":"Классика Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"25":{"src":"/images/calendar/block/25.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"130":{"text":"Нет","value":"130","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"131":{"text":"1 поле","value":"131","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"132":{"text":"Нет","value":"132","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"133":{"text":"1 поле","value":"133","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"134":{"text":"3 поля","value":"134","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"25","text":"Классика Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"26":{"src":"/images/calendar/block/26.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"137":{"text":"Нет","value":"137","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"138":{"text":"1 поле","value":"138","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"139":{"text":"Нет","value":"139","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"140":{"text":"1 поле","value":"140","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"141":{"text":"3 поля","value":"141","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"26","text":"Классика Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":3770},"27":{"src":"/images/calendar/block/27.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"144":{"text":"Нет","value":"144","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"145":{"text":"1 поле","value":"145","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"146":{"text":"Нет","value":"146","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"147":{"text":"1 поле","value":"147","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"148":{"text":"3 поля","value":"148","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"27","text":"Классика Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":3770},"28":{"src":"/images/calendar/block/28.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"151":{"text":"Нет","value":"151","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"152":{"text":"1 поле","value":"152","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"153":{"text":"Нет","value":"153","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"154":{"text":"1 поле","value":"154","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"155":{"text":"3 поля","value":"155","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"28","text":"Классика Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":3770},"29":{"src":"/images/calendar/block/29.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"263":{"text":"Нет","value":"263","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"264":{"text":"1 поле","value":"264","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"265":{"text":"Нет","value":"265","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"266":{"text":"1 поле","value":"266","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"267":{"text":"3 поля","value":"267","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"29","text":"Классика М Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":4000},"30":{"src":"/images/calendar/block/30.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"270":{"text":"Нет","value":"270","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"271":{"text":"1 поле","value":"271","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"272":{"text":"Нет","value":"272","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"273":{"text":"1 поле","value":"273","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"274":{"text":"3 поля","value":"274","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"30","text":"Классика М Серебристо-серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"7","price":4000},"31":{"src":"/images/calendar/block/31.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"277":{"text":"Нет","value":"277","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"278":{"text":"1 поле","value":"278","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"279":{"text":"Нет","value":"279","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"280":{"text":"1 поле","value":"280","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"281":{"text":"3 поля","value":"281","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"31","text":"Классика М Серебристо-белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"6","price":4000},"32":{"src":"/images/calendar/block/32.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"284":{"text":"Нет","value":"284","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"285":{"text":"1 поле","value":"285","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"286":{"text":"Нет","value":"286","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"287":{"text":"1 поле","value":"287","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"288":{"text":"3 поля","value":"288","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"32","text":"Классика Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"33":{"src":"/images/calendar/block/33.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"291":{"text":"Нет","value":"291","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"292":{"text":"1 поле","value":"292","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"293":{"text":"Нет","value":"293","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"294":{"text":"1 поле","value":"294","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"295":{"text":"3 поля","value":"295","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"33","text":"Классика Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"34":{"src":"/images/calendar/block/34.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"298":{"text":"Нет","value":"298","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"299":{"text":"1 поле","value":"299","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"300":{"text":"Нет","value":"300","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"301":{"text":"1 поле","value":"301","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"302":{"text":"3 поля","value":"302","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"34","text":"Классика Супер-металлик Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"12","price":4000},"35":{"src":"/images/calendar/block/35.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"312":{"text":"Нет","value":"312","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"313":{"text":"1 поле","value":"313","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"314":{"text":"Нет","value":"314","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"315":{"text":"1 поле","value":"315","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"316":{"text":"3 поля","value":"316","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"35","text":"Классика Супер-металлик Синий","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"11","price":4000},"37":{"src":"/images/calendar/block/37.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"158":{"text":"Нет","value":"158","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"159":{"text":"1 поле","value":"159","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"160":{"text":"Нет","value":"160","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"161":{"text":"1 поле","value":"161","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"162":{"text":"3 поля","value":"162","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"37","text":"Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"38":{"src":"/images/calendar/block/38.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"326":{"text":"Нет","value":"326","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"327":{"text":"1 поле","value":"327","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"328":{"text":"Нет","value":"328","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"329":{"text":"1 поле","value":"329","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"330":{"text":"3 поля","value":"330","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"38","text":"Болд Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"39":{"src":"/images/calendar/block/39.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"333":{"text":"Нет","value":"333","block_price":"40.00","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"334":{"text":"1 поле","value":"334","block_price":"40.00","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"335":{"text":"Нет","value":"335","block_price":"40.00","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"336":{"text":"1 поле","value":"336","block_price":"40.00","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"337":{"text":"3 поля","value":"337","block_price":"40.00","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"39","text":"Болд Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"40":{"src":"/images/calendar/block/40.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"165":{"text":"Нет","value":"165","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"166":{"text":"1 поле","value":"166","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"167":{"text":"Нет","value":"167","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"168":{"text":"1 поле","value":"168","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"169":{"text":"3 поля","value":"169","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"40","text":"Европа Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"41":{"src":"/images/calendar/block/41.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"172":{"text":"Нет","value":"172","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"173":{"text":"1 поле","value":"173","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"174":{"text":"Нет","value":"174","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"175":{"text":"1 поле","value":"175","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"176":{"text":"3 поля","value":"176","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"41","text":"Европа Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"42":{"src":"/images/calendar/block/42.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"179":{"text":"Нет","value":"179","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"180":{"text":"1 поле","value":"180","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"181":{"text":"Нет","value":"181","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"182":{"text":"1 поле","value":"182","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"183":{"text":"3 поля","value":"183","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"42","text":"Европа Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":3770},"43":{"src":"/images/calendar/block/43.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"186":{"text":"Нет","value":"186","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"187":{"text":"1 поле","value":"187","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"188":{"text":"Нет","value":"188","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"189":{"text":"1 поле","value":"189","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"190":{"text":"3 поля","value":"190","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"43","text":"Европа Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":3770},"44":{"src":"/images/calendar/block/44.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"193":{"text":"Нет","value":"193","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"194":{"text":"1 поле","value":"194","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"195":{"text":"Нет","value":"195","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"196":{"text":"1 поле","value":"196","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"197":{"text":"3 поля","value":"197","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"44","text":"Европа Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":3770},"45":{"src":"/images/calendar/block/45.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"200":{"text":"Нет","value":"200","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"201":{"text":"1 поле","value":"201","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"202":{"text":"Нет","value":"202","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"203":{"text":"1 поле","value":"203","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"204":{"text":"3 поля","value":"204","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"45","text":"Европа Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":3770},"46":{"src":"/images/calendar/block/46.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"207":{"text":"Нет","value":"207","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"208":{"text":"1 поле","value":"208","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"209":{"text":"Нет","value":"209","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"210":{"text":"1 поле","value":"210","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"211":{"text":"3 поля","value":"211","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"46","text":"Новая волна Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"47":{"src":"/images/calendar/block/47.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"214":{"text":"Нет","value":"214","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"215":{"text":"1 поле","value":"215","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"216":{"text":"Нет","value":"216","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"217":{"text":"1 поле","value":"217","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"218":{"text":"3 поля","value":"218","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"47","text":"Новая волна Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"48":{"src":"/images/calendar/block/48.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"221":{"text":"Нет","value":"221","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"222":{"text":"1 поле","value":"222","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"223":{"text":"Нет","value":"223","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"224":{"text":"1 поле","value":"224","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"225":{"text":"3 поля","value":"225","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"48","text":"Новая волна Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":3770},"49":{"src":"/images/calendar/block/49.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"228":{"text":"Нет","value":"228","block_price":"37.70","preparing_price":"2100.00","preparing_price_300":"11100.00","unit_price":"54.20","unit_price_300":"24.20","code":"0"},"229":{"text":"1 поле","value":"229","block_price":"37.70","preparing_price":"4100.00","preparing_price_300":"11600.00","unit_price":"63.20","unit_price_300":"38.20","code":"1"}}},"3":{"text":"3 пружины","value":"3","fields":{"230":{"text":"Нет","value":"230","block_price":"37.70","preparing_price":"3600.00","preparing_price_300":"9900.00","unit_price":"58.20","unit_price_300":"36.20","code":"0"},"231":{"text":"1 поле","value":"231","block_price":"37.70","preparing_price":"5400.00","preparing_price_300":"11400.00","unit_price":"62.20","unit_price_300":"42.20","code":"1"},"232":{"text":"3 поля","value":"232","block_price":"37.70","preparing_price":"7200.00","preparing_price_300":"12900.00","unit_price":"65.20","unit_price_300":"46.20","code":"3"}}}},"value":"49","text":"Новая волна Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":3770}},"code":"mini"},"2":{"text":"3 в 1 МИНИ","value":"2","blocks":{"50":{"src":"/images/calendar/block/50.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"340":{"text":"Нет","value":"340","block_price":"20.50","preparing_price":"2000.00","preparing_price_300":"12500.00","unit_price":"63.50","unit_price_300":"28.50","code":"0"},"341":{"text":"1 поле","value":"341","block_price":"20.50","preparing_price":"4200.00","preparing_price_300":"14100.00","unit_price":"72.50","unit_price_300":"39.50","code":"1"}}}},"value":"50","text":"3 в 1 Голубой","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"3","price":2050},"51":{"src":"/images/calendar/block/51.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"342":{"text":"Нет","value":"342","block_price":"20.50","preparing_price":"2000.00","preparing_price_300":"12500.00","unit_price":"63.50","unit_price_300":"28.50","code":"0"},"343":{"text":"1 поле","value":"343","block_price":"20.50","preparing_price":"4200.00","preparing_price_300":"14100.00","unit_price":"72.50","unit_price_300":"39.50","code":"1"}}}},"value":"51","text":"3 в 1 Серый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"2","price":2050},"52":{"src":"/images/calendar/block/52.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"345":{"text":"Нет","value":"345","block_price":"20.50","preparing_price":"2000.00","preparing_price_300":"12500.00","unit_price":"63.50","unit_price_300":"28.50","code":"0"},"346":{"text":"1 поле","value":"346","block_price":"20.50","preparing_price":"4200.00","preparing_price_300":"14100.00","unit_price":"72.50","unit_price_300":"39.50","code":"1"}}}},"value":"52","text":"3 в 1 Зеленый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"4","price":2050},"53":{"src":"/images/calendar/block/53.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"348":{"text":"Нет","value":"348","block_price":"20.50","preparing_price":"2000.00","preparing_price_300":"12500.00","unit_price":"63.50","unit_price_300":"28.50","code":"0"},"349":{"text":"1 поле","value":"349","block_price":"20.50","preparing_price":"4200.00","preparing_price_300":"14100.00","unit_price":"72.50","unit_price_300":"39.50","code":"1"}}}},"value":"53","text":"3 в 1 Бежевый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"5","price":2050}},"code":"3in1_mini"}},"code":"mini"},"3":{"name":"МИДИ","description":"335 мм","sizes":{"1":{"text":"МИНИ","value":"1","blocks":{"2":{"src":"/images/calendar/block/2.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"351":{"text":"3 поля","value":"351","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"2","text":"ПАРТНЕР Стандарт Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3080},"3":{"src":"/images/calendar/block/3.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"352":{"text":"3 поля","value":"352","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"3","text":"ПАРТНЕР Стандарт Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3080},"4":{"src":"/images/calendar/block/4.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"353":{"text":"3 поля","value":"353","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"4","text":"Супер Болд Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3080},"5":{"src":"/images/calendar/block/5.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"354":{"text":"3 поля","value":"354","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"5","text":"Супер Болд Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3080},"6":{"src":"/images/calendar/block/6.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"355":{"text":"3 поля","value":"355","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"6","text":"Супер Болд Зеленый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"4","price":3080},"7":{"src":"/images/calendar/block/7.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"358":{"text":"3 поля","value":"358","block_price":"32.50","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"7","text":"Вердана Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3250},"8":{"src":"/images/calendar/block/8.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"359":{"text":"3 поля","value":"359","block_price":"32.50","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"8","text":"Вердана Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3250},"9":{"src":"/images/calendar/block/9.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"360":{"text":"3 поля","value":"360","block_price":"32.50","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"9","text":"Вердана Белый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"1","price":3250},"14":{"src":"/images/calendar/block/14.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"356":{"text":"3 поля","value":"356","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"14","text":"Классика Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3080},"15":{"src":"/images/calendar/block/15.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"357":{"text":"3 поля","value":"357","block_price":"30.80","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"15","text":"Классика Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3080},"16":{"src":"/images/calendar/block/16.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"365":{"text":"3 поля","value":"365","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"16","text":"ПАРТНЕР Дизайн Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"17":{"src":"/images/calendar/block/17.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"366":{"text":"3 поля","value":"366","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"17","text":"ПАРТНЕР Дизайн Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"18":{"src":"/images/calendar/block/18.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"367":{"text":"3 поля","value":"367","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"18","text":"ПАРТНЕР Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"19":{"src":"/images/calendar/block/19.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"368":{"text":"3 поля","value":"368","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"19","text":"ПАРТНЕР Болд Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"20":{"src":"/images/calendar/block/20.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"385":{"text":"3 поля","value":"385","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"20","text":"ПАРТНЕР Металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"21":{"src":"/images/calendar/block/21.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"386":{"text":"3 поля","value":"386","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"21","text":"ПАРТНЕР Металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"22":{"src":"/images/calendar/block/22.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"387":{"text":"3 поля","value":"387","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"22","text":"ПАРТНЕР Болд Металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"23":{"src":"/images/calendar/block/23.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"388":{"text":"3 поля","value":"388","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"23","text":"ПАРТНЕР Болд Металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"24":{"src":"/images/calendar/block/24.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"369":{"text":"3 поля","value":"369","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"24","text":"Классика Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"25":{"src":"/images/calendar/block/25.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"370":{"text":"3 поля","value":"370","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"25","text":"Классика Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"26":{"src":"/images/calendar/block/26.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"371":{"text":"3 поля","value":"371","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"26","text":"Классика Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":3770},"27":{"src":"/images/calendar/block/27.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"372":{"text":"3 поля","value":"372","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"27","text":"Классика Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":3770},"28":{"src":"/images/calendar/block/28.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"373":{"text":"3 поля","value":"373","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"28","text":"Классика Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":3770},"29":{"src":"/images/calendar/block/29.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"389":{"text":"3 поля","value":"389","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"29","text":"Классика М Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":4000},"30":{"src":"/images/calendar/block/30.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"390":{"text":"3 поля","value":"390","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"30","text":"Классика М Серебристо-серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"7","price":4000},"31":{"src":"/images/calendar/block/31.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"391":{"text":"3 поля","value":"391","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"31","text":"Классика М Серебристо-белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"6","price":4000},"32":{"src":"/images/calendar/block/32.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"392":{"text":"3 поля","value":"392","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"32","text":"Классика Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"33":{"src":"/images/calendar/block/33.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"393":{"text":"3 поля","value":"393","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"33","text":"Классика Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"34":{"src":"/images/calendar/block/34.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"394":{"text":"3 поля","value":"394","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"34","text":"Классика Супер-металлик Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"12","price":4000},"35":{"src":"/images/calendar/block/35.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"395":{"text":"3 поля","value":"395","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"35","text":"Классика Супер-металлик Синий","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"11","price":4000},"37":{"src":"/images/calendar/block/37.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"374":{"text":"3 поля","value":"374","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"37","text":"Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"38":{"src":"/images/calendar/block/38.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"397":{"text":"3 поля","value":"397","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"38","text":"Болд Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4000},"39":{"src":"/images/calendar/block/39.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"398":{"text":"3 поля","value":"398","block_price":"40.00","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"39","text":"Болд Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4000},"40":{"src":"/images/calendar/block/40.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"375":{"text":"3 поля","value":"375","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"40","text":"Европа Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"41":{"src":"/images/calendar/block/41.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"376":{"text":"3 поля","value":"376","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"41","text":"Европа Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"42":{"src":"/images/calendar/block/42.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"377":{"text":"3 поля","value":"377","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"42","text":"Европа Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":3770},"43":{"src":"/images/calendar/block/43.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"378":{"text":"3 поля","value":"378","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"43","text":"Европа Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":3770},"44":{"src":"/images/calendar/block/44.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"379":{"text":"3 поля","value":"379","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"44","text":"Европа Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":3770},"45":{"src":"/images/calendar/block/45.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"380":{"text":"3 поля","value":"380","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"45","text":"Европа Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":3770},"46":{"src":"/images/calendar/block/46.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"381":{"text":"3 поля","value":"381","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"46","text":"Новая волна Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":3770},"47":{"src":"/images/calendar/block/47.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"382":{"text":"3 поля","value":"382","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"47","text":"Новая волна Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":3770},"48":{"src":"/images/calendar/block/48.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"383":{"text":"3 поля","value":"383","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"48","text":"Новая волна Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":3770},"49":{"src":"/images/calendar/block/49.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"384":{"text":"3 поля","value":"384","block_price":"37.70","preparing_price":"7600.00","preparing_price_300":"48700.00","unit_price":"157.20","unit_price_300":"20.20","code":"3"}}}},"value":"49","text":"Новая волна Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":3770}},"code":"mini"},"2":{"text":"3 в 1 МИНИ","value":"2","blocks":{"50":{"src":"/images/calendar/block/50.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"483":{"text":"1 поле","value":"483","block_price":"20.50","preparing_price":"4000.00","preparing_price_300":"26200.00","unit_price":"94.50","unit_price_300":"20.50","code":"1"}}}},"value":"50","text":"3 в 1 Голубой","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"3","price":2050},"51":{"src":"/images/calendar/block/51.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"484":{"text":"1 поле","value":"484","block_price":"20.50","preparing_price":"4000.00","preparing_price_300":"26200.00","unit_price":"94.50","unit_price_300":"20.50","code":"1"}}}},"value":"51","text":"3 в 1 Серый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"2","price":2050},"52":{"src":"/images/calendar/block/52.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"485":{"text":"1 поле","value":"485","block_price":"20.50","preparing_price":"4000.00","preparing_price_300":"26200.00","unit_price":"94.50","unit_price_300":"20.50","code":"1"}}}},"value":"52","text":"3 в 1 Зеленый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"4","price":2050},"53":{"src":"/images/calendar/block/53.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"486":{"text":"1 поле","value":"486","block_price":"20.50","preparing_price":"4000.00","preparing_price_300":"26200.00","unit_price":"94.50","unit_price_300":"20.50","code":"1"}}}},"value":"53","text":"3 в 1 Бежевый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"5","price":2050}},"code":"3in1_mini"},"3":{"text":"МИДИ","value":"3","blocks":{"4":{"src":"/images/calendar/block/4.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"399":{"text":"Нет","value":"399","block_price":"35.40","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"400":{"text":"1 поле","value":"400","block_price":"35.40","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"401":{"text":"3 поля","value":"401","block_price":"35.40","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"4","text":"Супер Болд Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3540},"5":{"src":"/images/calendar/block/5.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"402":{"text":"Нет","value":"402","block_price":"35.40","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"403":{"text":"1 поле","value":"403","block_price":"35.40","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"404":{"text":"3 поля","value":"404","block_price":"35.40","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"5","text":"Супер Болд Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3540},"6":{"src":"/images/calendar/block/6.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"405":{"text":"Нет","value":"405","block_price":"35.40","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"406":{"text":"1 поле","value":"406","block_price":"35.40","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"407":{"text":"3 поля","value":"407","block_price":"35.40","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"6","text":"Супер Болд Зеленый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"4","price":3540},"7":{"src":"/images/calendar/block/7.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"408":{"text":"Нет","value":"408","block_price":"37.70","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"409":{"text":"1 поле","value":"409","block_price":"37.70","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"410":{"text":"3 поля","value":"410","block_price":"37.70","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"7","text":"Вердана Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3770},"8":{"src":"/images/calendar/block/8.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"411":{"text":"Нет","value":"411","block_price":"37.70","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"412":{"text":"1 поле","value":"412","block_price":"37.70","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"413":{"text":"3 поля","value":"413","block_price":"37.70","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"8","text":"Вердана Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3770},"9":{"src":"/images/calendar/block/9.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"414":{"text":"Нет","value":"414","block_price":"37.70","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"415":{"text":"1 поле","value":"415","block_price":"37.70","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"416":{"text":"3 поля","value":"416","block_price":"37.70","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"9","text":"Вердана Белый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"1","price":3770},"24":{"src":"/images/calendar/block/24.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"417":{"text":"Нет","value":"417","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"418":{"text":"1 поле","value":"418","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"419":{"text":"3 поля","value":"419","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"24","text":"Классика Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":4290},"25":{"src":"/images/calendar/block/25.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"420":{"text":"Нет","value":"420","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"421":{"text":"1 поле","value":"421","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"422":{"text":"3 поля","value":"422","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"25","text":"Классика Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":4290},"26":{"src":"/images/calendar/block/26.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"423":{"text":"Нет","value":"423","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"424":{"text":"1 поле","value":"424","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"425":{"text":"3 поля","value":"425","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"26","text":"Классика Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":4290},"27":{"src":"/images/calendar/block/27.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"426":{"text":"Нет","value":"426","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"427":{"text":"1 поле","value":"427","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"428":{"text":"3 поля","value":"428","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"27","text":"Классика Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":4290},"28":{"src":"/images/calendar/block/28.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"429":{"text":"Нет","value":"429","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"430":{"text":"1 поле","value":"430","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"431":{"text":"3 поля","value":"431","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"28","text":"Классика Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":4290},"29":{"src":"/images/calendar/block/29.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"453":{"text":"Нет","value":"453","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"454":{"text":"1 поле","value":"454","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"455":{"text":"3 поля","value":"455","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"29","text":"Классика М Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":4580},"30":{"src":"/images/calendar/block/30.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"456":{"text":"Нет","value":"456","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"457":{"text":"1 поле","value":"457","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"458":{"text":"3 поля","value":"458","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"30","text":"Классика М Серебристо-серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"7","price":4580},"31":{"src":"/images/calendar/block/31.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"459":{"text":"Нет","value":"459","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"460":{"text":"1 поле","value":"460","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"461":{"text":"3 поля","value":"461","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"31","text":"Классика М Серебристо-белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"6","price":4580},"32":{"src":"/images/calendar/block/32.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"462":{"text":"Нет","value":"462","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"463":{"text":"1 поле","value":"463","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"464":{"text":"3 поля","value":"464","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"32","text":"Классика Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4580},"33":{"src":"/images/calendar/block/33.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"465":{"text":"Нет","value":"465","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"466":{"text":"1 поле","value":"466","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"467":{"text":"3 поля","value":"467","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"33","text":"Классика Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4580},"34":{"src":"/images/calendar/block/34.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"468":{"text":"Нет","value":"468","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"469":{"text":"1 поле","value":"469","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"470":{"text":"3 поля","value":"470","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"34","text":"Классика Супер-металлик Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"12","price":4580},"35":{"src":"/images/calendar/block/35.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"471":{"text":"Нет","value":"471","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"472":{"text":"1 поле","value":"472","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"473":{"text":"3 поля","value":"473","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"35","text":"Классика Супер-металлик Синий","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"11","price":4580},"37":{"src":"/images/calendar/block/37.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"432":{"text":"Нет","value":"432","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"433":{"text":"1 поле","value":"433","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"434":{"text":"3 поля","value":"434","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"37","text":"Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":4290},"38":{"src":"/images/calendar/block/38.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"477":{"text":"Нет","value":"477","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"478":{"text":"1 поле","value":"478","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"479":{"text":"3 поля","value":"479","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"38","text":"Болд Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4580},"39":{"src":"/images/calendar/block/39.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"480":{"text":"Нет","value":"480","block_price":"45.80","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"481":{"text":"1 поле","value":"481","block_price":"45.80","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"482":{"text":"3 поля","value":"482","block_price":"45.80","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"39","text":"Болд Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4580},"40":{"src":"/images/calendar/block/40.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"435":{"text":"Нет","value":"435","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"436":{"text":"1 поле","value":"436","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"437":{"text":"3 поля","value":"437","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"40","text":"Европа Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":4290},"41":{"src":"/images/calendar/block/41.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"438":{"text":"Нет","value":"438","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"439":{"text":"1 поле","value":"439","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"440":{"text":"3 поля","value":"440","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"41","text":"Европа Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":4290},"42":{"src":"/images/calendar/block/42.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"441":{"text":"Нет","value":"441","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"442":{"text":"1 поле","value":"442","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"443":{"text":"3 поля","value":"443","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"42","text":"Европа Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":4290},"43":{"src":"/images/calendar/block/43.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"447":{"text":"Нет","value":"447","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"448":{"text":"1 поле","value":"448","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"449":{"text":"3 поля","value":"449","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"43","text":"Европа Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":4290},"44":{"src":"/images/calendar/block/44.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"444":{"text":"Нет","value":"444","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"445":{"text":"1 поле","value":"445","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"446":{"text":"3 поля","value":"446","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"44","text":"Европа Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":4290},"45":{"src":"/images/calendar/block/45.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"450":{"text":"Нет","value":"450","block_price":"42.90","preparing_price":"2800.00","preparing_price_300":"24700.00","unit_price":"92.60","unit_price_300":"19.60","code":"0"},"451":{"text":"1 поле","value":"451","block_price":"42.90","preparing_price":"5100.00","preparing_price_300":"36300.00","unit_price":"124.60","unit_price_300":"20.60","code":"1"},"452":{"text":"3 поля","value":"452","block_price":"42.90","preparing_price":"7000.00","preparing_price_300":"48400.00","unit_price":"157.60","unit_price_300":"19.60","code":"3"}}}},"value":"45","text":"Европа Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":4290}},"code":"midi"},"4":{"text":"3 в 1 МИДИ","value":"4","blocks":{"50":{"src":"/images/calendar/block/50.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"487":{"text":"Нет","value":"487","block_price":"22.80","preparing_price":"3300.00","preparing_price_300":"20700.00","unit_price":"76.20","unit_price_300":"18.20","code":"0"},"488":{"text":"1 поле","value":"488","block_price":"22.80","preparing_price":"4600.00","preparing_price_300":"25000.00","unit_price":"92.20","unit_price_300":"24.20","code":"1"}}}},"value":"50","text":"3 в 1 Голубой","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"3","price":2280},"51":{"src":"/images/calendar/block/51.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"489":{"text":"Нет","value":"489","block_price":"22.80","preparing_price":"3300.00","preparing_price_300":"20700.00","unit_price":"76.20","unit_price_300":"18.20","code":"0"},"490":{"text":"1 поле","value":"490","block_price":"22.80","preparing_price":"4600.00","preparing_price_300":"25000.00","unit_price":"92.20","unit_price_300":"24.20","code":"1"}}}},"value":"51","text":"3 в 1 Серый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"2","price":2280},"52":{"src":"/images/calendar/block/52.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"492":{"text":"Нет","value":"492","block_price":"22.80","preparing_price":"3300.00","preparing_price_300":"20700.00","unit_price":"76.20","unit_price_300":"18.20","code":"0"},"493":{"text":"1 поле","value":"493","block_price":"22.80","preparing_price":"4600.00","preparing_price_300":"25000.00","unit_price":"92.20","unit_price_300":"24.20","code":"1"}}}},"value":"52","text":"3 в 1 Зеленый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"4","price":2280},"53":{"src":"/images/calendar/block/53.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"495":{"text":"Нет","value":"495","block_price":"22.80","preparing_price":"3300.00","preparing_price_300":"20700.00","unit_price":"76.20","unit_price_300":"18.20","code":"0"},"496":{"text":"1 поле","value":"496","block_price":"22.80","preparing_price":"4600.00","preparing_price_300":"25000.00","unit_price":"92.20","unit_price_300":"24.20","code":"1"}}}},"value":"53","text":"3 в 1 Бежевый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"5","price":2280}},"code":"3in1_midi"}},"code":"midi"},"4":{"name":"МАКСИ","description":"370 мм","sizes":{"3":{"text":"МИДИ","value":"3","blocks":{"4":{"src":"/images/calendar/block/4.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"498":{"text":"3 поля","value":"498","block_price":"35.40","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"4","text":"Супер Болд Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3540},"5":{"src":"/images/calendar/block/5.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"499":{"text":"3 поля","value":"499","block_price":"35.40","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"5","text":"Супер Болд Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3540},"6":{"src":"/images/calendar/block/6.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"500":{"text":"3 поля","value":"500","block_price":"35.40","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"6","text":"Супер Болд Зеленый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"4","price":3540},"7":{"src":"/images/calendar/block/7.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"501":{"text":"3 поля","value":"501","block_price":"37.70","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"7","text":"Вердана Голубой","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"3","price":3770},"8":{"src":"/images/calendar/block/8.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"502":{"text":"3 поля","value":"502","block_price":"37.70","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"8","text":"Вердана Серый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"2","price":3770},"9":{"src":"/images/calendar/block/9.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"503":{"text":"3 поля","value":"503","block_price":"37.70","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"9","text":"Вердана Белый","is_ofset":"1","is_3in1":"0","is_mel":"0","color":"1","price":3770},"24":{"src":"/images/calendar/block/24.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"504":{"text":"3 поля","value":"504","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"24","text":"Классика Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":4290},"25":{"src":"/images/calendar/block/25.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"505":{"text":"3 поля","value":"505","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"25","text":"Классика Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":4290},"26":{"src":"/images/calendar/block/26.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"506":{"text":"3 поля","value":"506","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"26","text":"Классика Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":4290},"27":{"src":"/images/calendar/block/27.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"507":{"text":"3 поля","value":"507","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"27","text":"Классика Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":4290},"28":{"src":"/images/calendar/block/28.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"508":{"text":"3 поля","value":"508","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"28","text":"Классика Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":4290},"29":{"src":"/images/calendar/block/29.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"516":{"text":"3 поля","value":"516","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"29","text":"Классика М Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":4580},"30":{"src":"/images/calendar/block/30.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"517":{"text":"3 поля","value":"517","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"30","text":"Классика М Серебристо-серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"7","price":4580},"31":{"src":"/images/calendar/block/31.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"518":{"text":"3 поля","value":"518","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"31","text":"Классика М Серебристо-белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"6","price":4580},"32":{"src":"/images/calendar/block/32.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"519":{"text":"3 поля","value":"519","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"32","text":"Классика Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4580},"33":{"src":"/images/calendar/block/33.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"520":{"text":"3 поля","value":"520","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"33","text":"Классика Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4580},"34":{"src":"/images/calendar/block/34.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"521":{"text":"3 поля","value":"521","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"34","text":"Классика Супер-металлик Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"12","price":4580},"35":{"src":"/images/calendar/block/35.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"522":{"text":"3 поля","value":"522","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"35","text":"Классика Супер-металлик Синий","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"11","price":4580},"37":{"src":"/images/calendar/block/37.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"509":{"text":"3 поля","value":"509","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"37","text":"Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":4290},"38":{"src":"/images/calendar/block/38.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"524":{"text":"3 поля","value":"524","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"38","text":"Болд Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":4580},"39":{"src":"/images/calendar/block/39.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"525":{"text":"3 поля","value":"525","block_price":"45.80","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"39","text":"Болд Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":4580},"40":{"src":"/images/calendar/block/40.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"510":{"text":"3 поля","value":"510","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"40","text":"Европа Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":4290},"41":{"src":"/images/calendar/block/41.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"511":{"text":"3 поля","value":"511","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"41","text":"Европа Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":4290},"42":{"src":"/images/calendar/block/42.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"512":{"text":"3 поля","value":"512","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"42","text":"Европа Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":4290},"43":{"src":"/images/calendar/block/43.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"514":{"text":"3 поля","value":"514","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"43","text":"Европа Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":4290},"44":{"src":"/images/calendar/block/44.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"513":{"text":"3 поля","value":"513","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"44","text":"Европа Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":4290},"45":{"src":"/images/calendar/block/45.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"515":{"text":"3 поля","value":"515","block_price":"42.90","preparing_price":"11100.00","preparing_price_300":"42300.00","unit_price":"126.60","unit_price_300":"22.60","code":"3"}}}},"value":"45","text":"Европа Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":4290}},"code":"midi"},"4":{"text":"3 в 1 МИДИ","value":"4","blocks":{"50":{"src":"/images/calendar/block/50.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"592":{"text":"1 поле","value":"592","block_price":"22.80","preparing_price":"5600.00","preparing_price_300":"29600.00","unit_price":"98.20","unit_price_300":"18.20","code":"1"}}}},"value":"50","text":"3 в 1 Голубой","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"3","price":2280},"51":{"src":"/images/calendar/block/51.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"593":{"text":"1 поле","value":"593","block_price":"22.80","preparing_price":"5600.00","preparing_price_300":"29600.00","unit_price":"98.20","unit_price_300":"18.20","code":"1"}}}},"value":"51","text":"3 в 1 Серый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"2","price":2280},"52":{"src":"/images/calendar/block/52.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"594":{"text":"1 поле","value":"594","block_price":"22.80","preparing_price":"5600.00","preparing_price_300":"29600.00","unit_price":"98.20","unit_price_300":"18.20","code":"1"}}}},"value":"52","text":"3 в 1 Зеленый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"4","price":2280},"53":{"src":"/images/calendar/block/53.jpg","springs":{"1":{"text":"1 пружина","value":"1","fields":{"595":{"text":"1 поле","value":"595","block_price":"22.80","preparing_price":"5600.00","preparing_price_300":"29600.00","unit_price":"98.20","unit_price_300":"18.20","code":"1"}}}},"value":"53","text":"3 в 1 Бежевый","is_ofset":"0","is_3in1":"1","is_mel":"0","color":"5","price":2280}},"code":"3in1_midi"},"5":{"text":"МАКСИ","value":"5","blocks":{"24":{"src":"/images/calendar/block/24.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"526":{"text":"Нет","value":"526","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"527":{"text":"1 поле","value":"527","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"528":{"text":"3 поля","value":"528","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"24","text":"Классика Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":5960},"25":{"src":"/images/calendar/block/25.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"529":{"text":"Нет","value":"529","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"530":{"text":"1 поле","value":"530","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"531":{"text":"3 поля","value":"531","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"25","text":"Классика Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":5960},"26":{"src":"/images/calendar/block/26.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"532":{"text":"Нет","value":"532","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"533":{"text":"1 поле","value":"533","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"534":{"text":"3 поля","value":"534","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"26","text":"Классика Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":5960},"27":{"src":"/images/calendar/block/27.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"535":{"text":"Нет","value":"535","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"536":{"text":"1 поле","value":"536","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"537":{"text":"3 поля","value":"537","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"27","text":"Классика Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":5960},"28":{"src":"/images/calendar/block/28.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"538":{"text":"Нет","value":"538","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"539":{"text":"1 поле","value":"539","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"540":{"text":"3 поля","value":"540","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"28","text":"Классика Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":5960},"29":{"src":"/images/calendar/block/29.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"562":{"text":"Нет","value":"562","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"563":{"text":"1 поле","value":"563","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"564":{"text":"3 поля","value":"564","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"29","text":"Классика М Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":6300},"30":{"src":"/images/calendar/block/30.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"565":{"text":"Нет","value":"565","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"566":{"text":"1 поле","value":"566","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"567":{"text":"3 поля","value":"567","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"30","text":"Классика М Серебристо-серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"7","price":6300},"31":{"src":"/images/calendar/block/31.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"568":{"text":"Нет","value":"568","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"569":{"text":"1 поле","value":"569","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"570":{"text":"3 поля","value":"570","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"31","text":"Классика М Серебристо-белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"6","price":6300},"32":{"src":"/images/calendar/block/32.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"571":{"text":"Нет","value":"571","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"572":{"text":"1 поле","value":"572","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"573":{"text":"3 поля","value":"573","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"32","text":"Классика Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":6300},"33":{"src":"/images/calendar/block/33.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"574":{"text":"Нет","value":"574","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"575":{"text":"1 поле","value":"575","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"576":{"text":"3 поля","value":"576","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"33","text":"Классика Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":6300},"34":{"src":"/images/calendar/block/34.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"577":{"text":"Нет","value":"577","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"578":{"text":"1 поле","value":"578","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"579":{"text":"3 поля","value":"579","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"34","text":"Классика Супер-металлик Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"12","price":6300},"35":{"src":"/images/calendar/block/35.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"580":{"text":"Нет","value":"580","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"581":{"text":"1 поле","value":"581","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"582":{"text":"3 поля","value":"582","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"35","text":"Классика Супер-металлик Синий","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"11","price":6300},"37":{"src":"/images/calendar/block/37.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"541":{"text":"Нет","value":"541","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"542":{"text":"1 поле","value":"542","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"543":{"text":"3 поля","value":"543","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"37","text":"Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":5960},"38":{"src":"/images/calendar/block/38.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"586":{"text":"Нет","value":"586","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"587":{"text":"1 поле","value":"587","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"588":{"text":"3 поля","value":"588","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"38","text":"Болд Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":6300},"39":{"src":"/images/calendar/block/39.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"589":{"text":"Нет","value":"589","block_price":"63.00","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"590":{"text":"1 поле","value":"590","block_price":"63.00","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"591":{"text":"3 поля","value":"591","block_price":"63.00","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"39","text":"Болд Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":6300},"40":{"src":"/images/calendar/block/40.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"544":{"text":"Нет","value":"544","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"545":{"text":"1 поле","value":"545","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"546":{"text":"3 поля","value":"546","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"40","text":"Европа Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":5960},"41":{"src":"/images/calendar/block/41.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"547":{"text":"Нет","value":"547","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"548":{"text":"1 поле","value":"548","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"549":{"text":"3 поля","value":"549","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"41","text":"Европа Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":5960},"42":{"src":"/images/calendar/block/42.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"550":{"text":"Нет","value":"550","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"551":{"text":"1 поле","value":"551","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"552":{"text":"3 поля","value":"552","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"42","text":"Европа Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":5960},"43":{"src":"/images/calendar/block/43.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"553":{"text":"Нет","value":"553","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"554":{"text":"1 поле","value":"554","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"555":{"text":"3 поля","value":"555","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"43","text":"Европа Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":5960},"44":{"src":"/images/calendar/block/44.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"556":{"text":"Нет","value":"556","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"557":{"text":"1 поле","value":"557","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"558":{"text":"3 поля","value":"558","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"44","text":"Европа Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":5960},"45":{"src":"/images/calendar/block/45.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"559":{"text":"Нет","value":"559","block_price":"59.60","preparing_price":"5100.00","preparing_price_300":"25200.00","unit_price":"86.40","unit_price_300":"19.40","code":"0"},"560":{"text":"1 поле","value":"560","block_price":"59.60","preparing_price":"8600.00","preparing_price_300":"32900.00","unit_price":"102.40","unit_price_300":"21.40","code":"1"},"561":{"text":"3 поля","value":"561","block_price":"59.60","preparing_price":"11600.00","preparing_price_300":"42800.00","unit_price":"126.40","unit_price_300":"22.40","code":"3"}}}},"value":"45","text":"Европа Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":5960}},"code":"maxi"}},"code":"maxi"},"5":{"name":"МАКСИ +","description":"410 мм","sizes":{"5":{"text":"МАКСИ","value":"5","blocks":{"24":{"src":"/images/calendar/block/24.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"596":{"text":"3 поля","value":"596","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"24","text":"Классика Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":5960},"25":{"src":"/images/calendar/block/25.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"597":{"text":"3 поля","value":"597","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"25","text":"Классика Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":5960},"26":{"src":"/images/calendar/block/26.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"598":{"text":"3 поля","value":"598","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"26","text":"Классика Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":5960},"27":{"src":"/images/calendar/block/27.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"599":{"text":"3 поля","value":"599","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"27","text":"Классика Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":5960},"28":{"src":"/images/calendar/block/28.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"600":{"text":"3 поля","value":"600","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"28","text":"Классика Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":5960},"29":{"src":"/images/calendar/block/29.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"608":{"text":"3 поля","value":"608","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"29","text":"Классика М Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":6300},"30":{"src":"/images/calendar/block/30.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"609":{"text":"3 поля","value":"609","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"30","text":"Классика М Серебристо-серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"7","price":6300},"31":{"src":"/images/calendar/block/31.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"610":{"text":"3 поля","value":"610","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"31","text":"Классика М Серебристо-белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"6","price":6300},"32":{"src":"/images/calendar/block/32.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"611":{"text":"3 поля","value":"611","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"32","text":"Классика Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":6300},"33":{"src":"/images/calendar/block/33.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"612":{"text":"3 поля","value":"612","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"33","text":"Классика Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":6300},"34":{"src":"/images/calendar/block/34.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"613":{"text":"3 поля","value":"613","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"34","text":"Классика Супер-металлик Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"12","price":6300},"35":{"src":"/images/calendar/block/35.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"614":{"text":"3 поля","value":"614","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"35","text":"Классика Супер-металлик Синий","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"11","price":6300},"37":{"src":"/images/calendar/block/37.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"601":{"text":"3 поля","value":"601","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"37","text":"Болд Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":5960},"38":{"src":"/images/calendar/block/38.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"616":{"text":"3 поля","value":"616","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"38","text":"Болд Супер-металлик Золото","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"10","price":6300},"39":{"src":"/images/calendar/block/39.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"617":{"text":"3 поля","value":"617","block_price":"63.00","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"39","text":"Болд Супер-металлик Серебро","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"9","price":6300},"40":{"src":"/images/calendar/block/40.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"602":{"text":"3 поля","value":"602","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"40","text":"Европа Голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"3","price":5960},"41":{"src":"/images/calendar/block/41.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"603":{"text":"3 поля","value":"603","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"41","text":"Европа Серый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"2","price":5960},"42":{"src":"/images/calendar/block/42.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"604":{"text":"3 поля","value":"604","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"42","text":"Европа Зеленый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"4","price":5960},"43":{"src":"/images/calendar/block/43.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"605":{"text":"3 поля","value":"605","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"43","text":"Европа Бежевый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"5","price":5960},"44":{"src":"/images/calendar/block/44.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"606":{"text":"3 поля","value":"606","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"44","text":"Европа Серебристо-голубой","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"8","price":5960},"45":{"src":"/images/calendar/block/45.jpg","springs":{"3":{"text":"3 пружины","value":"3","fields":{"607":{"text":"3 поля","value":"607","block_price":"59.60","preparing_price":"12600.00","preparing_price_300":"50400.00","unit_price":"153.40","unit_price_300":"25.40","code":"3"}}}},"value":"45","text":"Европа Белый","is_ofset":"0","is_3in1":"0","is_mel":"1","color":"1","price":5960}},"code":"maxi"}},"code":"maxi_plus"}}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _select = __webpack_require__(3);

var _select2 = _interopRequireDefault(_select);

var _result = __webpack_require__(8);

var _result2 = _interopRequireDefault(_result);

var _link = __webpack_require__(9);

var _link2 = _interopRequireDefault(_link);

var _kind = __webpack_require__(10);

var _kind2 = _interopRequireDefault(_kind);

var _preview = __webpack_require__(15);

var _preview2 = _interopRequireDefault(_preview);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PRINT_COUNTS = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 800, 1000, 1200, 1500, 2000];
var DEFAULT_PRINT_COUNT = 150;

var Calc = function () {
    function Calc(data, options) {
        _classCallCheck(this, Calc);

        Object.assign(this, { data: data, options: options });
        this.init();
    }

    _createClass(Calc, [{
        key: 'init',
        value: function init() {
            var _this = this;

            //#print_count
            this.print_count = new _select2.default('#print_count', PRINT_COUNTS.map(function (value) {
                return { text: value + ' \u0448\u0442', value: value, selected: value == DEFAULT_PRINT_COUNT };
            })).on('selected', function (d) {
                return _this.calc();
            });

            this.field = new _select2.default('#field', [], 'Выберите кол-во полей').on('selected', function () {
                return _this.calc();
            });
            this.spring = new _select2.default('#spring', [], 'Выберите кол-во пружин').on('selected', function (_ref) {
                var fields = _ref.fields;
                return _this.field.init(_this._objToArray(fields));
            });
            this.kind = new _kind2.default('#kind_select', '#calc_container', this.options.BASE_URL).on('selected', function (_ref2) {
                var springs = _ref2.springs;
                return _this.spring.init(_this._objToArray(springs));
            });
            this.format = new _select2.default('#format', [], 'Выберите формат').on('selected', function (_ref3) {
                var blocks = _ref3.blocks;
                return _this.kind.set(_this._objToArray(blocks));
            });
            this.poster = new _select2.default('#poster', this.posters, 'Выберите размер').on('selected', function (_ref4) {
                var sizes = _ref4.sizes;
                return _this.format.init(_this._objToArray(sizes));
            });

            this.links = {
                online: new _link2.default('#submit_online', this.options.BASE_URL, 'calendar/upload').disable(),
                email: new _link2.default('#submit_email', this.options.BASE_URL, 'calendar/email').disable()
            };
            this.results = {
                online: new _result2.default('#price_online'),
                email: new _result2.default('#price_email'),
                office: new _result2.default('#price_office')
            };
            this.preview = new _preview2.default('#preview', this.options.BASE_URL);
            this.poster.touch();
        }
    }, {
        key: '_objToArray',
        value: function _objToArray(obj) {
            var results = [];
            var i = 0;
            for (var pN in obj) {
                obj[pN].index = i++;
                results.push(obj[pN]);
            }
            return results;
        }
    }, {
        key: 'calc',
        value: function calc() {
            var count = parseInt(this.print_count.value);
            if (count && this.field.value) {
                var block_price = parseFloat(this.field.data.block_price);
                var unit_price = count > 300 ? parseFloat(this.field.data.unit_price_300) : parseFloat(this.field.data.unit_price);
                var preparing_price = count > 300 ? parseFloat(this.field.data.preparing_price_300) : parseFloat(this.field.data.preparing_price);

                var price = preparing_price + block_price * count + unit_price * count;
                this.results.online.set(price);
                this.results.email.set(price + 500);
                this.results.office.set(price + 1000);

                this.preview.src('/images/calendar-previews/' + this.poster.data.code + '_' + this.format.data.code + '_' + this.spring.value + '_' + this.field.data.code);

                this.links.online.change({ fid: this.field.value, c: count }).enable();
                this.links.email.change({ fid: this.field.value, c: count }).enable();
                this.preview.show();
            } else {
                this.links.online.disable();
                this.links.email.disable();
                this.preview.hide();
            }
        }
    }, {
        key: 'posters',
        get: function get() {
            var results = [];
            for (var value in this.data) {
                var _data$value = this.data[value],
                    name = _data$value.name,
                    code = _data$value.code,
                    sizes = _data$value.sizes;

                results.push({ text: name, value: value, code: code, sizes: sizes });
            }
            return results;
        }
    }]);

    return Calc;
}();

exports.default = Calc;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Result = function (_Element) {
    _inherits(Result, _Element);

    function Result() {
        _classCallCheck(this, Result);

        return _possibleConstructorReturn(this, (Result.__proto__ || Object.getPrototypeOf(Result)).apply(this, arguments));
    }

    _createClass(Result, [{
        key: 'set',
        value: function set(value) {
            this.el.text(value);
            return this;
        }
    }]);

    return Result;
}(_element2.default);

exports.default = Result;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Link = function (_Element) {
    _inherits(Link, _Element);

    function Link(selector, BASE_URL, PATH) {
        var enabled = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

        _classCallCheck(this, Link);

        var _this = _possibleConstructorReturn(this, (Link.__proto__ || Object.getPrototypeOf(Link)).call(this, selector));

        Object.assign(_this, { BASE_URL: BASE_URL, PATH: PATH, enabled: enabled });
        _this.el.on('click', function () {
            return _this.enabled;
        });
        return _this;
    }

    _createClass(Link, [{
        key: 'buildUrl',
        value: function buildUrl(baseUrl, options) {
            var url = baseUrl + options.path;
            var delimeter = '?';
            for (var pN in options.queryParams) {
                url += delimeter + pN + '=' + encodeURIComponent(options.queryParams[pN]);
                delimeter = '&';
            }
            return url;
        }
    }, {
        key: 'change',
        value: function change(queryParams) {
            var url = this.buildUrl(this.BASE_URL, {
                path: this.PATH,
                hash: '',
                queryParams: queryParams
            });
            this.el.attr('href', url);
            return this;
        }
    }, {
        key: 'enable',
        value: function enable() {
            this.el.removeClass('gray');
            this.enabled = true;
            return this;
        }
    }, {
        key: 'disable',
        value: function disable() {
            this.el.addClass('gray');
            this.enabled = false;
            return this;
        }
    }]);

    return Link;
}(_element2.default);

exports.default = Link;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

var _select = __webpack_require__(3);

var _select2 = _interopRequireDefault(_select);

var _label = __webpack_require__(11);

var _label2 = _interopRequireDefault(_label);

var _browserEventEmitter = __webpack_require__(2);

var _browserEventEmitter2 = _interopRequireDefault(_browserEventEmitter);

var _card = __webpack_require__(12);

var _card2 = _interopRequireDefault(_card);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _fake = __webpack_require__(13);

var _fake2 = _interopRequireDefault(_fake);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var COLORS = ["Не задан", "Белый", "Серый", "Голубой", "Зеленый", "Бежевый", "Серебристо-белый", "Серебристо-серый", "Серебристо-голубой", "Серебро", "Золото", "Синий металлик", "Зеленый металлик", "Бронза"];

var Container = function (_Element) {
    _inherits(Container, _Element);

    function Container(selector, BASE_URL) {
        _classCallCheck(this, Container);

        var _this = _possibleConstructorReturn(this, (Container.__proto__ || Object.getPrototypeOf(Container)).call(this, selector));

        Object.assign(_this, { BASE_URL: BASE_URL });
        return _this;
    }

    _createClass(Container, [{
        key: 'generate',
        value: function generate(data) {
            var _this2 = this;

            var html = '<div>';
            data.forEach(function (item) {
                html += (0, _card2.default)({ item: Object.assign({}, item, { src: _this2.BASE_URL + item.src }) });
            });
            this.el.html(html + '</div><div class="clearfix"/>');
        }
    }]);

    return Container;
}(_element2.default);

var Kind = function (_Element2) {
    _inherits(Kind, _Element2);

    function Kind(selector, selector2, BASE_URL) {
        _classCallCheck(this, Kind);

        var _this3 = _possibleConstructorReturn(this, (Kind.__proto__ || Object.getPrototypeOf(Kind)).call(this, selector));

        window.kind = _this3;
        Object.assign(_this3, { events: new _browserEventEmitter2.default(), BASE_URL: BASE_URL, calc: new _element2.default(selector2) });
        _this3.init();
        return _this3;
    }

    _createClass(Kind, [{
        key: 'show',
        value: function show() {
            _get(Kind.prototype.__proto__ || Object.getPrototypeOf(Kind.prototype), 'show', this).call(this);
            this.calc.hide();
        }
    }, {
        key: 'hide',
        value: function hide() {
            _get(Kind.prototype.__proto__ || Object.getPrototypeOf(Kind.prototype), 'hide', this).call(this);
            this.calc.show();
        }
    }, {
        key: 'on',
        value: function on(name, callback) {
            this.events.on(name, callback);
            return this;
        }
    }, {
        key: 'onSelect',
        value: function onSelect(data) {
            this.select.text(data.text);
            this.events.emit('selected', data);
            return this.hide();
        }
    }, {
        key: 'map',
        value: function map(data, callback) {
            var results = [];
            for (var pN in data) {
                results.push(callback(data[pN], pN));
            }
            return results;
        }
    }, {
        key: 'set',
        value: function set(data) {
            this.data = data;
            var assoc = {};
            this.data.forEach(function (item) {
                assoc[item.price] = item.price;
            });
            var prices = [{ text: 'Стоимость', value: -1 }].concat(_toConsumableArray(this.map(assoc, function (value) {
                return { text: (value / 100).toFixed(2), value: value };
            })));
            this.price.init(prices);

            this.generate();

            var _ref = data || [],
                _ref2 = _slicedToArray(_ref, 1),
                first = _ref2[0];

            if (first) {
                this.onSelect(first);
            }
            return this;
        }
    }, {
        key: 'generate',
        value: function generate() {
            //filters
            var color = this.color.value;
            var price = this.price.value;
            var data = this.filter(this.data, function (item) {
                return color == item.color || color == 0;
            });
            data = this.filter(data, function (item) {
                return price == item.price || price == -1;
            });

            var ofsetData = this.filter(data, function (item) {
                return item.is_ofset == 1;
            });
            this.containers.ofset.generate(ofsetData);
            this.labels.ofset.visible(ofsetData.length);

            var melData = this.filter(data, function (item) {
                return item.is_mel == 1;
            });
            this.containers.mel.generate(melData);
            this.labels.mel.visible(melData.length);

            var d3in1Data = this.filter(data, function (item) {
                return item.is_3in1 == 1;
            });
            this.containers.d3in1.generate(d3in1Data);
            this.labels.d3in1.visible(d3in1Data.length);

            return this;
        }
    }, {
        key: 'filter',
        value: function filter(data, callback) {
            var results = [];
            for (var pN in data) {
                var item = data[pN];
                if (callback(item, pN)) {
                    results.push(item);
                }
            }
            return results;
        }
    }, {
        key: 'init',
        value: function init() {
            var _this4 = this;

            this.select = new _fake2.default('#kind', 'Выберите дизайн').on('click', function (d) {
                _this4.show();
            });

            this.containers = {
                ofset: new Container('#kind_ofset_container', this.BASE_URL),
                mel: new Container('#kind_mel_container', this.BASE_URL),
                d3in1: new Container('#kind_3in1_container', this.BASE_URL)
            };
            this.labels = {
                ofset: new _label2.default('#kind_ofset_label'),
                mel: new _label2.default('#kind_mel_label'),
                d3in1: new _label2.default('#kind_3in1_label')
            };

            var options = { width: '100px' };
            this.color = new _select2.default('#kind_color', COLORS.map(function (text, value) {
                return { text: text, value: value };
            }), 'Цвет', options).on('selected', function () {
                return _this4.generate();
            });
            this.price = new _select2.default('#kind_price', [], 'Стоимость', options).on('selected', function () {
                return _this4.generate();
            });
        }
    }]);

    return Kind;
}(_element2.default);

exports.default = Kind;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Label = function (_Element) {
    _inherits(Label, _Element);

    function Label() {
        _classCallCheck(this, Label);

        return _possibleConstructorReturn(this, (Label.__proto__ || Object.getPrototypeOf(Label)).apply(this, arguments));
    }

    _createClass(Label, [{
        key: 'visible',
        value: function visible(value) {
            if (value) this.show();else this.hide();
        }
    }]);

    return Label;
}(_element2.default);

exports.default = Label;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var pug = __webpack_require__(4);

function template(locals) {var pug_html = "", pug_mixins = {}, pug_interp;;var locals_for_with = (locals || {});(function (JSON, Math, item) {var pug_indent = [];
pug_html = pug_html + "\n\u003Cdiv class=\"card\"\u003E\u003Ca" + (pug.attr("href", "javascript: kind.onSelect(" + JSON.stringify(item) + ")", true, true)) + "\u003E\u003Ci class=\"card-a-bg\"\u003E \u003C\u002Fi\u003E\u003Cb class=\"card-a-btn\"\u003E\u003Ci class=\"nobr\"\u003EВыбрать\u003C\u002Fi\u003E\u003C\u002Fb\u003E\n    \u003Cdiv class=\"imm\"\u003E\u003Cspan class=\"name\"\u003E" + (pug.escape(null == (pug_interp = item.text) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003Cimg" + (" alt=\"\""+pug.attr("src", item.src, true, true)) + "\u003E\u003Cspan class=\"price\"\u003E " + (pug.escape(null == (pug_interp = Math.floor(item.price/100)) ? "" : pug_interp)) + "\u003Csup\u003E" + (pug.escape(null == (pug_interp = Math.round(item.price % 100)) ? "" : pug_interp)) + "р.\u003C\u002Fsup\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E\u003C\u002Fdiv\u003E";}.call(this,"JSON" in locals_for_with?locals_for_with.JSON:typeof JSON!=="undefined"?JSON:undefined,"Math" in locals_for_with?locals_for_with.Math:typeof Math!=="undefined"?Math:undefined,"item" in locals_for_with?locals_for_with.item:typeof item!=="undefined"?item:undefined));;return pug_html;};
module.exports = template;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

var _fakeSelect = __webpack_require__(14);

var _fakeSelect2 = _interopRequireDefault(_fakeSelect);

var _browserEventEmitter = __webpack_require__(2);

var _browserEventEmitter2 = _interopRequireDefault(_browserEventEmitter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FakeSelect = function (_Element) {
    _inherits(FakeSelect, _Element);

    function FakeSelect(selector, selectText) {
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : { width: '200px' };

        _classCallCheck(this, FakeSelect);

        var _this = _possibleConstructorReturn(this, (FakeSelect.__proto__ || Object.getPrototypeOf(FakeSelect)).call(this, selector));

        _this.init();
        Object.assign(options, { selectText: selectText });
        Object.assign(_this, { events: new _browserEventEmitter2.default(), options: options });
        return _this;
    }

    _createClass(FakeSelect, [{
        key: 'on',
        value: function on(eventName, callback) {
            this.events.on(eventName, callback);
            return this;
        }
    }, {
        key: 'text',
        value: function text(value) {
            this.el.find('label.dd-selected-text').text(value);
            return this;
        }
    }, {
        key: 'init',
        value: function init() {
            var _this2 = this;

            this.el.html((0, _fakeSelect2.default)(this.options));
            this.el.click(function (e) {
                return _this2.events.emit('click', {});
            });
        }
    }]);

    return FakeSelect;
}(_element2.default);

exports.default = FakeSelect;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var pug = __webpack_require__(4);

function template(locals) {var pug_html = "", pug_mixins = {}, pug_interp;;var locals_for_with = (locals || {});(function (selectText, width) {var pug_indent = [];
pug_html = pug_html + "\n\u003Cdiv" + (" class=\"dd-select\""+pug.attr("style", pug.style("width: " + width + 9 +"px; background: rgb(238, 238, 238);"), true, true)) + "\u003E\u003Ca class=\"dd-selected\"\u003E\n    \u003Clabel class=\"dd-selected-text\"\u003E" + (pug.escape(null == (pug_interp = selectText) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E\u003C\u002Fa\u003E\u003Cspan class=\"dd-pointer dd-pointer-down\"\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E";}.call(this,"selectText" in locals_for_with?locals_for_with.selectText:typeof selectText!=="undefined"?selectText:undefined,"width" in locals_for_with?locals_for_with.width:typeof width!=="undefined"?width:undefined));;return pug_html;};
module.exports = template;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _element = __webpack_require__(0);

var _element2 = _interopRequireDefault(_element);

__webpack_require__(18);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Preview = function (_Element) {
    _inherits(Preview, _Element);

    function Preview(selector, BASE_URL) {
        _classCallCheck(this, Preview);

        var _this = _possibleConstructorReturn(this, (Preview.__proto__ || Object.getPrototypeOf(Preview)).call(this, selector));

        Object.assign(_this, { BASE_URL: BASE_URL, img: _this.el.find('img') });
        return _this;
    }

    _createClass(Preview, [{
        key: 'src',
        value: function src(_src) {
            this.el.attr('href', this.BASE_URL + _src + '.jpg');
            this.img.attr('src', this.BASE_URL + _src + '-low.jpg');
            return this;
        }
    }]);

    return Preview;
}(_element2.default);

exports.default = Preview;

/***/ }),
/* 16 */,
/* 17 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),
/* 18 */,
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(5);

var _data = __webpack_require__(6);

var _data2 = _interopRequireDefault(_data);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _calc = __webpack_require__(7);

var _calc2 = _interopRequireDefault(_calc);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var options = {
    BASE_URL: 'http://www.kalendar.printer-net.ru/'
};

(0, _jquery2.default)(function () {
    return new _calc2.default(_data2.default, options);
});

/***/ })
],[19]);
//# sourceMappingURL=kalendar.bundle.js.map