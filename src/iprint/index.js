
import '../common/main.scss';
import data from '../data.json';
import $ from 'jquery';
import Calc from '../common/components/calc';

const options = {
    BASE_URL: 'http://www.iprint.printer-net.ru/',
}

$(() => new Calc(data, options))