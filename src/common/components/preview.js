import Element from './element';
import 'fancybox';

export default class Preview extends Element {
    constructor(selector, BASE_URL) {
        super(selector);
        Object.assign(this, {BASE_URL, img: this.el.find('img')});
    }
    src(src) {
        this.el.attr('href', `${this.BASE_URL + src}.jpg`);
        this.img.attr('src', `${this.BASE_URL + src}-low.jpg`);
        return this;
    }
}