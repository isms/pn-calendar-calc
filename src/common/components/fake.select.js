import Element from './element';
import template from '../parts/fake.select.pug';
import EventEmitter from 'browser-event-emitter';

export default class FakeSelect extends Element {
    constructor(selector, selectText, options = {width: '200px'}) {
        super(selector);
        this.init();
        Object.assign(options, {selectText});
        Object.assign(this, {events: new EventEmitter(), options})
    }
    on(eventName, callback) {
        this.events.on(eventName, callback)
        return this;
    }
    text(value) {
        this.el.find('label.dd-selected-text').text(value);
        return this;
    }
    init() {
        this.el.html(template(this.options));
        this.el.click((e) => this.events.emit('click', {}));
    }
}