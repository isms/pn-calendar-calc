import Element from './element';

export default class Label extends Element {
    visible(value) {
        if (value)
            this.show()
        else 
            this.hide()
    }
}