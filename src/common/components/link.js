import Element from './element';

export default class Link extends Element {
    constructor(selector, BASE_URL, PATH, enabled = true) {
        super(selector);
        Object.assign(this, {BASE_URL, PATH, enabled})
        this.el.on('click', () => this.enabled)
    }
    buildUrl(baseUrl, options) {
        let url = baseUrl + options.path;
        let delimeter = '?';
        for (var pN in options.queryParams) {
            url += delimeter + pN + '=' + encodeURIComponent(options.queryParams[pN]);
            delimeter = '&';
        }
        return url;
    }
    change(queryParams) {
        const url = this.buildUrl(this.BASE_URL, {
            path: this.PATH,
            hash: '',
            queryParams,
        })
        this.el.attr('href', url);
        return this;
    }
    enable() {
        this.el.removeClass('gray');
        this.enabled = true;
        return this;
    }
    disable() {
        this.el.addClass('gray');
        this.enabled = false;
        return this;
    }
}