import $ from 'jquery';

export default class Element {
    constructor(selector) {
        Object.assign(this, {el: $(selector), selector});
    }
    show() {
        this.el.show();
        return this;
    }
    hide() {
        this.el.hide();
        return this;
    }
}