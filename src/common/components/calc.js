import Select from './select';
import Result from './result';
import Link from './link';
import Kind from './kind';
import Preview from './preview';
const PRINT_COUNTS = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 800, 1000, 1200, 1500, 2000];
const DEFAULT_PRINT_COUNT = 150;

export default class Calc {
    constructor(data, options) {
        Object.assign(this, {data, options});
        this.init()
    }
    get posters() {
        let results = [];
        for( let value in this.data) {
            const {name, code, sizes} = this.data[value];
            results.push({text: name, value, code, sizes});
        }
        return results;
    }
    init() {
        //#print_count
        this.print_count = (new Select('#print_count', PRINT_COUNTS.map(value => ({text: `${value} шт`, value, selected: value == DEFAULT_PRINT_COUNT})))).on('selected', d => this.calc());

        this.field = (new Select('#field', [], 'Выберите кол-во полей')).on('selected', () => this.calc());
        this.spring = (new Select('#spring', [], 'Выберите кол-во пружин')).on('selected', ({fields}) => this.field.init(this._objToArray(fields)));
        this.kind = (new Kind('#kind_select', '#calc_container', this.options.BASE_URL)).on('selected', ({springs}) => this.spring.init(this._objToArray(springs)));
        this.format = (new Select('#format', [], 'Выберите формат')).on('selected', ({blocks}) => this.kind.set(this._objToArray(blocks)));
        this.poster = (new Select('#poster', this.posters, 'Выберите размер')).on('selected', ({sizes}) => this.format.init(this._objToArray(sizes)))
        
        this.links = {
            online: (new Link('#submit_online', this.options.BASE_URL, 'calendar/upload')).disable(),
            email: (new Link('#submit_email', this.options.BASE_URL, 'calendar/email')).disable(),
        }
        this.results = {
            online: new Result('#price_online'),
            email: new Result('#price_email'),
            office: new Result('#price_office'),
        }
        this.preview = new Preview('#preview', this.options.BASE_URL);
        this.poster.touch();
    }
    _objToArray(obj) {
        let results = [];
        let i = 0;
        for (let pN in obj) {
            obj[pN].index = i++;
            results.push(obj[pN]);
        }
        return results;
    }
    calc() {
        const count = parseInt(this.print_count.value);
        if (count && this.field.value) {
            const block_price = parseFloat(this.field.data.block_price);
            const unit_price = count > 300 ? parseFloat(this.field.data.unit_price_300) : parseFloat(this.field.data.unit_price);
            const preparing_price = count > 300 ? parseFloat(this.field.data.preparing_price_300) :parseFloat(this.field.data.preparing_price);

            const price = preparing_price + block_price * count + unit_price * count;
            this.results.online.set(price);
            this.results.email.set(price + 500);
            this.results.office.set(price + 1000);

            this.preview.src(`/images/calendar-previews/${this.poster.data.code}_${this.format.data.code}_${this.spring.value}_${this.field.data.code}`);

            this.links.online.change({fid: this.field.value, c: count}).enable();
            this.links.email.change({fid: this.field.value, c: count}).enable();
            this.preview.show();
        } else {
            this.links.online.disable();
            this.links.email.disable();
            this.preview.hide();
        }
    }
}