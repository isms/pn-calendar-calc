import Element from './element';
import Select from './select';
import Label from './label';
import EventEmitter from 'browser-event-emitter';
import template from '../parts/card.pug';
import $ from 'jquery';
import FakeSelect from './fake.select';

const COLORS = [
    "Не задан",
    "Белый",
    "Серый",
    "Голубой",
    "Зеленый",
    "Бежевый",
    "Серебристо-белый",
    "Серебристо-серый",
    "Серебристо-голубой",
    "Серебро",
    "Золото",
    "Синий металлик",
    "Зеленый металлик",
    "Бронза",
];

class Container extends Element {
    constructor(selector, BASE_URL) {
        super(selector);
        Object.assign(this, {BASE_URL});
    }
    generate(data) {
        let html = '<div>';
        data.forEach((item) => {
            html += template({item: Object.assign({}, item, {src: this.BASE_URL + item.src})});
        });
        this.el.html(html + '</div><div class="clearfix"/>');
    }
    
}
export default class Kind extends Element {
    constructor(selector, selector2, BASE_URL) {
        super(selector);
        window.kind = this;
        Object.assign(this, {events: new EventEmitter(), BASE_URL, calc: new Element(selector2)});
        this.init();
    }
    show() {
        super.show();
        this.calc.hide();
    }
    hide() {
        super.hide();
        this.calc.show();
    }
    on(name, callback) {
        this.events.on(name, callback);
        return this;
    }
    onSelect(data) {
        this.select.text(data.text);
        this.events.emit('selected', data);
        return this.hide();
    }
    map(data, callback) {
        let results = [];
        for (let pN in data) {
            results.push(callback(data[pN], pN));
        }
        return results;
    }
    set(data) {
        this.data = data;
        let assoc = {};
        this.data.forEach(item => {
            assoc[item.price] = item.price;
        });
        let prices = [{text: 'Стоимость', value: -1}, ...this.map(assoc, (value) => ({text: (value/100).toFixed(2), value}))];
        this.price.init(prices);

        this.generate();
        const [first] = data || [];
        if (first) {
            this.onSelect(first)
        }
        return this;
    }
    generate() {
        //filters
        const color = this.color.value;
        const price = this.price.value;
        let data = this.filter(this.data, item => color == item.color || color == 0);
        data = this.filter(data, item => price == item.price || price == -1);

        const ofsetData = this.filter(data, item => item.is_ofset == 1);
        this.containers.ofset.generate(ofsetData);
        this.labels.ofset.visible(ofsetData.length);

        const melData = this.filter(data, item => item.is_mel == 1);
        this.containers.mel.generate(melData);
        this.labels.mel.visible(melData.length);

        const d3in1Data = this.filter(data, item => item.is_3in1 == 1);
        this.containers.d3in1.generate(d3in1Data);
        this.labels.d3in1.visible(d3in1Data.length);

        return this;
    }
    filter(data, callback) {
        let results = [];
        for (let pN in data) {
            const item = data[pN];
            if (callback(item, pN)) {
                results.push(item);
            }
        }
        return results;
    }
    init() {
        this.select = (new FakeSelect('#kind', 'Выберите дизайн')).on('click', d => {
            this.show();
        });
        
        this.containers = {
            ofset: new Container('#kind_ofset_container', this.BASE_URL),
            mel: new Container('#kind_mel_container', this.BASE_URL),
            d3in1: new Container('#kind_3in1_container', this.BASE_URL),
        };
        this.labels = {
            ofset: new Label('#kind_ofset_label'),
            mel: new Label('#kind_mel_label'),
            d3in1: new Label('#kind_3in1_label'),
        };

        const options = {width: '100px'};
        this.color = (new Select('#kind_color', COLORS.map((text, value) => ({text, value})), 'Цвет', options)).on('selected', () => this.generate());
        this.price = (new Select('#kind_price', [], 'Стоимость', options)).on('selected', () => this.generate());
    }
}