import Element from './element';
import EventEmitter from 'browser-event-emitter';
import 'ddslick';
import $ from 'jquery';

export default class Select extends Element {
    constructor(selector, data = [], selectText = 'Выберите', options = {width: '209px'}) {
        super(selector);
        Object.assign(options, {selectText, onSelected: ({selectedData}) => this.touch(Object.assign(this, {selectedData})), truncateDescription: true})
        Object.assign(this, {events: new EventEmitter(), options})
        this.init(data);
    }
    touch() {
        this.events.emit('selected', this.data);
        return this;
    }
    on(eventName, callback) {
        this.events.on(eventName, callback)
        return this;
    }
    close() {
        this.el.ddslick('close');
        return this;
    }
    get data() {
        return this.selectedData || $(this.selector).data('ddslick') || {value: null};
    }
    get value() {
        return this.data.value;
    }
    select(index) {
        if (this.options.data.length && this.options.data[index]) {
            $(this.selector).ddslick('select', {index});
            this.touch(this.selectedData = this.options.data[index]);
        }
        return this;
    }
    init(data = [], index = 0) {
        Object.assign(this.options, {data});
        //bug fix
        $(this.selector).ddslick('destroy');
        // console.log(this.constructor.name, this.el.attr('id'), data);
        $(this.selector).empty();
        $(this.selector).ddslick(this.options);
        return this.select(index);
    }
}