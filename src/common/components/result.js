import Element from './element';

export default class Result extends Element {
    set(value) {
        this.el.text(value);
        return this;
    }
}