## Install
```shell
yarn install
```
## development
```shell
yarn build:watch
```
## serve
```shell
yarn serve
```
## build
```shell
yarn build:clean
yarn build
yarn build:production
```
