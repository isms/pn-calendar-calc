//build enviroments
const path = require('path');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
let src = path.resolve('./src');
const codes = ['kalendar', 'irm', 'iprint', 'main'];

//configuration
module.exports = {
  entry: {
    kalendar: path.join(src, 'kalendar/index.js'),
    irm: path.join(src, 'irm/index.js'),
    main: path.join(src, 'main/index.js'),
    iprint: path.join(src, 'iprint/index.js'),
  },
  output: {
    path: path.join(__dirname, '..', 'dist'),
    filename: '[name]/[name].bundle.js',
    publicPath: '/',
  },
  resolve: {
    modules: [
      src,
      path.resolve('./node_modules'),
    ],
    alias: {
      ddslick: path.resolve('./node_modules/ddslick/dist/jquery.ddslick.min.js'),
    }
  },
  plugins: codes.map((id) => {
        return new HtmlWebpackPlugin({
          chunks: ['vendor', id],
          //main entry
          template: './src/' + id + '/index.pug',
          //result filename
          filename: id + '/index.html',
          cache: true,
          //inject favicon
          favicon: './src/favicon.ico',
          //inject into body
          inject: 'body',
          minify: {
            collapseWhitespace: false,
            //clean all comments
            removeComments: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          }
        })
    }).concat([
      //Setup additional options for loaders
      new webpack.LoaderOptionsPlugin({
        options: {
          context: src,
          postcss: [autoprefixer({browsers: 'last 2 versions'})],
        }
      }),
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
      }),
      /**
       * Separate vendor and application sources
       */
      new webpack.optimize.CommonsChunkPlugin({
          name: 'vendor',
          // this assumes your vendor imports exist in the node_modules directory
          minChunks: module => module.context && module.context.indexOf('node_modules') !== -1,
      }),
  ]),
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: '/node_modules/' },
      { test: /\.css$/, loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: `css-loader?${JSON.stringify({discardComments: {removeAll: true}})}!resolve-url-loader?import=false!postcss-loader`}) },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: `css-loader?${JSON.stringify({discardComments: {removeAll: true}})}!resolve-url-loader?sourceMap!postcss-loader!sass-loader?sourceMap`}) },
      { test: /\.less$/, loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: `css-loader?${JSON.stringify({discardComments: {removeAll: true}})}!resolve-url-loader?sourceMap!postcss-loader!less-loader?sourceMap`}) },
      { test: /\.pug$/, loader: `pug-loader?root=${src}&pretty=true` },
      //{ test: /\.html$/, loader: 'raw' },
      { test: /\.json$/, loader: 'json-loader?name=[path][name].[ext]' },
      // inline base64 URLs for <=8k images, direct URLs for the rest
      { test: /\.(png|jpg|webp)$/, loader: 'url-loader?limit=8192&name=images/[name]-[hash:10].[ext]'},
    ],
  },
};
