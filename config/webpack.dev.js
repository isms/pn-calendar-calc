const path = require('path');
const webpackMerge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const commonConfig = require('./webpack.common.js');
const HotModuleReplacementPlugin = require('webpack').HotModuleReplacementPlugin;
const argv = require('yargs').argv;
let plugins = [
    new ExtractTextPlugin({
        filename: '[name]/[name].css',
    }),
    // new HotModuleReplacementPlugin(),
];
const availables = {
  /**
   * Plugin: ngAnnotatePlugin
   * Description: Plugin to add auto injections
   */
  clean: new CleanWebpackPlugin(['dist'], {
    root: path.join(__dirname, '..'),
    verbose: true,
  }),
};
//attache all available plugins based on arguments
Object.keys(availables).filter(key => argv.env && argv.env[key]).forEach(key => plugins.push(availables[key]));

module.exports = webpackMerge(commonConfig, {
  devtool: 'source-map',
  plugins,
});
