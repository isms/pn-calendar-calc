switch (process.env.NODE_ENV) {
  case 'prod':
  case 'production':
    module.exports = require('./config/webpack.production');
    break;
  case 'clean': 
    module.exports = require('./config/webpack.clean')
    break;
  case 'dev':
  case 'development':
  default:
    module.exports = require('./config/webpack.dev');
}